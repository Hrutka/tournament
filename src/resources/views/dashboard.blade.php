@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Személyes adataid
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p>Név: {{ $user->name }}</p>
                        <p>Email: {{ $user->email }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">

                        <div class="row">
                            <div class="col">
                                Csapataid
                            </div>
                            <div class="col text-right">
                                <a href="/new-participant/0" class="btn btn-primary btn-sm">Új csapat</a>
                            </div>
                        </div>


                        {{--<span class="float-right text-primary">
                            <svg class="bi bi-plus-square-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" d="M2 0a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2H2zm6.5 4a.5.5 0 00-1 0v3.5H4a.5.5 0 000 1h3.5V12a.5.5 0 001 0V8.5H12a.5.5 0 000-1H8.5V4z" clip-rule="evenodd"/>
                            </svg>
                        </span>--}}
                    </div>

                    <div class="card-body">
                        @if (count($teams) === 0)
                            <p>Még nincs egy csapatod sem.</p>
                        @else
                            @foreach($teams as $team)
                                <div class="row pt-1 pb-1">
                                    <div class="col">
                                        {{ $team->display_name }} ({{ $team->users->count() }} csapattag)
                                    </div>
                                    @if(count($team->tournaments()->get()) == 0 and count($team->users()->get()) == 1)
                                        <div class="col text-right">
                                            <a href="/leave-participant/{{ $team->id }}" class="btn btn-danger btn-sm">Kilépés</a>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Mérkőzéseid</div>

                    <div class="card-body">
                        @if (count($matches) === 0)
                            <p>Még nem játszottál egy mérkőzést sem.</p>
                        @else
                            <ul>
                                @foreach($matches as $match)
                                    <li>
                                        {{ $match->homeParticipant->display_name }}
                                        vs {{ $match->guestParticipant->display_name }}
                                        @ {{ $match->tournament->title }}
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
