<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $teams = $user->participants()->get()->all();
        $matches = [];
        foreach ($user->participants()->get() as $team) {
            $matches = array_merge($matches, $team->matches()->all());
        }
        return view('dashboard', compact('user', 'teams', 'matches'));
    }
}
