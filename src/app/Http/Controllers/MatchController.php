<?php


namespace App\Http\Controllers;


use App\Match;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class MatchController extends Controller
{

    public function update(Request $request, $id)
    {
        $match = Match::where('id', $id)->firstOrFail();

        if (Auth::user()->id !== $match->tournament->owner) {
            throw new AccessDeniedException("Nincs megfelelő hozzáférésed");
        }

        if ($request->has('participant1') && !empty($request->participant1)) {
            $match->homeParticipant()->associate(
                Participant::where('id', $request->participant1)->firstOrFail()
            );
        }


        if ($request->has('participant2') && !empty($request->participant2)) {
            $match->guestParticipant()->associate(
                Participant::where('id', $request->participant2)->firstOrFail()
            );
        }

        if ($request->has('winner') && !empty($request->winner)) {
            $match->winner = $request->winner;
        }

        $match->save();

        return back();
    }
}
