# Tournament

## Database seeder

### Matches tábla

Az 5-ös tournament meccsei. 8 csapat, 7 meccs, 3 körben. Első körben az első participant nyer, második körben a második, harmadik körben újra az elő. Így a végső győztes a 2-es participant lesz.

### Users tábla

10db user, email: testuser'N'@gmai.com (N = 1..10), password: password.

### Tournaments tábla

10db tournament különböző számú csapatokkal (4/8/16 egyenlőre).

### Particpiants tábla

10 db participant. Különböző teamsize-al.

### Particpiant_tournament tábla

Az 5-ös tournamenthez tartozó participantok (1-8).

### User_participant tábla

Melyik userhez melyik particpiant tartozik. 
- user3: 2 participant(1,9)
- user2: 2 participant(3.10)
- user4: 0 participant
- user9: 0 participant
- mindenki más: 1-1 particiapnt.

Adatbázis feltöltése adatokkal(kb 10-10 sor):
- `cd docker`
- `docker-compose run php-fpm php artisan db:seed`

PhpMyAdmin elérés: `http://localhost:8080`

Kiszolgáló: `mysql`, felhasználónév: `default`, jelszó: `secret`