<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 5; $i++) {
            DB::table('matches')->insert([
                'created_at' => Carbon::now(),
                'participant1_id' => $i,
                'participant2_id' => 9 - $i,
                'round' => 1,
                'tournament_id' => 5,
                'updated_at' => Carbon::now(),
                'winner' => 1,
            ]);
        }

        DB::table('matches')->insert([
            'created_at' => Carbon::now(),
            'participant1_id' => 1,
            'participant2_id' => 2,
            'round' => 2,
            'tournament_id' => 5,
            'updated_at' => Carbon::now(),
            'winner' => 2,
        ]);

        DB::table('matches')->insert([
            'created_at' => Carbon::now(),
            'participant1_id' => 3,
            'participant2_id' => 4,
            'round' => 2,
            'tournament_id' => 5,
            'updated_at' => Carbon::now(),
            'winner' => 2,
        ]);

        DB::table('matches')->insert([
            'created_at' => Carbon::now(),
            'participant1_id' => 2,
            'participant2_id' => 4,
            'round' => 3,
            'tournament_id' => 5,
            'updated_at' => Carbon::now(),
            'winner' => 1,
        ]);

    }
}
