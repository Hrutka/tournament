@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Bajnokság hozzáadása</div>

                    <div class="card-body">
                        <form action="/save-tournament/{{ $id ?? '0' }}" method="post">

                            @csrf

                            <div class="form-group">
                                <label for="title">Tournament name</label>
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="title"
                                       required
                                       value="{{ $tournament->title ?? '' }}">
                            </div>

                            <div class="form-group">
                                <label for="description">Tournament description</label>
                                <textarea class="form-control" id="description" name="description"
                                          required
                                          aria-describedby="description">{{ $tournament->description ?? '' }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="team-size">Competing team size</label>
                                <select class="form-control" id="team-size" name="team_size" required>
                                    <option value="1" {{ (isset($tournament) and $tournament->team_size == 1) ? 'selected' : '' }}>1</option>
                                    <option value="2" {{ (isset($tournament) and $tournament->team_size == 2) ? 'selected' : '' }}>2</option>
                                    <option value="3" {{ (isset($tournament) and $tournament->team_size == 3) ? 'selected' : '' }}>3</option>
                                    <option value="4" {{ (isset($tournament) and $tournament->team_size == 4) ? 'selected' : '' }}>4</option>
                                    <option value="5" {{ (isset($tournament) and $tournament->team_size == 5) ? 'selected' : '' }}>5</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="tournament_size">Tournament size</label>
                                <select class="form-control" id="tournament_size" name="tournament_size" required>
                                    <option value="4" {{ (isset($tournament) and $tournament->tournament_size == 4) ? 'selected' : '' }}>4</option>
                                    <option value="8" {{ (isset($tournament) and $tournament->tournament_size == 8) ? 'selected' : '' }}>8</option>
                                    <option value="16" {{ (isset($tournament) and $tournament->tournament_size == 6) ? 'selected' : '' }}>16</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="start-date">Tournament start</label>
                                <input type="date" class="form-control" id="start-date"
                                       name="start_date" aria-describedby="start-date"
                                       required
                                       value="{{ (isset($tournament) and $tournament->start_date) ? date('Y-m-d', strtotime($tournament->start_date)) : '' }}">
                            </div>

                            <button type="submit" class="btn btn-primary">Létrehozás</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
