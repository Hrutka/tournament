<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'participant1_id', 'participant2_id', 'round', 'winner'
    ];
    /**
     *
     */
    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    /**
     *
     */
    public function homeParticipant()
    {
        return $this->belongsTo(Participant::class, 'participant1_id');
    }

    /**
     *
     */
    public function guestParticipant()
    {
        return $this->belongsTo(Participant::class, 'participant2_id');
    }
}
