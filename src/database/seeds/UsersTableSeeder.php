<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 11; $i++) {
            DB::table('users')->insert([
                'created_at' => Carbon::now(),
                'email' => sprintf('test_user%s@test.test', $i),
                'email_verified_at' => Carbon::now(),
                'name' => 'Test User'.$i,
                'password' => Hash::make('password'),
                'remember_token' => 'Remember Me'.$i,
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
