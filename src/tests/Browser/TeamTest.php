<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use mysql_xdevapi\Exception;
use Tests\DuskTestCase;

class TeamTest extends DuskTestCase
{
    private $name = 'mytestteam01';
    private $size = 3;

    /**
     *
     * @test
     * @return void
     * @throws \Throwable
     */
    public function mePageTest()
    {

        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            try {
                $browser->LoginAs($user->email)
                    ->visit('/')
                    ->clickLink($user->name)
                    ->clickLink('Dashboard')
                    ->assertPathIs('/me')
                    ->assertSee('Név: '. $user->name)
                    ->assertSee('Email: '. $user->email)
                    ->assertSeeLink('Új csapat');
            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $user->delete();
            }
        });
    }


    /**
     *
     * @test
     * @return void
     * @throws \Throwable
     */
    public function createAndDeleteTeamTest()
    {

        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            try {
                $browser->LoginAs($user->email)
                    ->visit('/me')
                    ->clickLink('Új csapat')
                    ->assertPathIs('/new-participant/0')
                    ->type('display_name', $this->name)
                    ->select('team_size', $this->size)
                    ->press('Létrehozás')
                    ->assertPathIs('/me')
                    ->assertSee($this->name.' (1 csapattag)')
                    ->assertSeeLink('Kilépés')
                    ->clickLink('Kilépés')
                    ->assertPathIs('/me')
                    ->assertDontSee($this->name.' (1 csapattag)')
                    ->assertDontSeeLink('Kilépés')
                    ->assertSee('Még nincs egy csapatod sem.')
                    ->assertSee('Még nem játszottál egy mérkőzést sem.');
            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $user->delete();
            }
        });
    }

    /**
     *
     * @test
     * @return void
     * @throws \Throwable
     */
    public function joinTeamTest()
    {

        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            try {
                $browser->LoginAs($user->email)
                    ->visit('/')
                    ->assertSeeLink('Csapatba jelentkezés')
                    ->clickLink('Csapatba jelentkezés')
                    ->assertDontSee('Még nincs egy csapatod sem.')
                    ->clickLink('Kilépés')
                    ->assertPathIs('/me')
                    ->assertSee('Még nincs egy csapatod sem.')
                    ->visit('/join-participant/1')
                    ->assertPathIs('/me')
                    ->assertSee('Participant 1')
                    ->clickLink('Kilépés');
            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $user->delete();
            }
        });
    }
}
