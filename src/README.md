# Tournament

## Mappaszerkezet

- `docker`: minden dockerrel kapcsolatos dolog
- `src`: laravel forráskód 

## Fejlesztés

### 1) Telepítés:
- `cp .env.example .env` (laravel környezeti változók)
- `composer install` (laravel függőségek telepítése)
- `npm install` (a frontend függőségek feltepítése)
- `npm run build` (a frontend buildelése)
- ha nincs a `.env` fájlban `APP_KEY` beállítva: `php artisan key:generate`

### 2) Indítás docker mappából:
- `docker-compose up -d` (elindítja a konténert a háttérben)
- `docker-compose run php-fpm php artisan migrate` (az adatbázis migrationök futtatása a php-fpm konténerben)

Böngészőből elérhető a `http://localhost`.

### 3) Adatbázisban mókolás:
PhpMyAdmin elérés: `http://localhost:8080`

kiszolgáló: `mysql`, felhasználónév: `default`, jelszó: `secret`
