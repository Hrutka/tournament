<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tournament;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Tournament::class, function (Faker $faker) {
    return [
        'start_date' => now(),
        'title' => $faker->word(),
        'description' => $faker->word(),
        'team_size' => 4,
    ];
});
