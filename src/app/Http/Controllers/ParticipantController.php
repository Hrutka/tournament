<?php

namespace App\Http\Controllers;

use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ParticipantController extends Controller
{
    public function addParticipant($id)
    {
        if ($id == 0) {
            return view('add_participant');

        } else {
            $participant = Participant::where('id', $id)->first();

            return view('add_participant', compact('id', 'participant'));
        }

    }

    public function saveParticipant(Request $request, $id = 0)
    {

        if ($id == 0) {
            $participant = new Participant();
        } else {
            $participant = Participant::where('id', $id)->first();
        }

        $participant->display_name = $request->display_name;
        $participant->team_size = $request->team_size;


        $participant->save();

        $participant->users()->attach(Auth::user());

        return redirect('/me');

    }

    public function joinParticipant($id)
    {
        $participant = Participant::where('id', $id)->first();

        $participant->users()->save(Auth::user());

        return redirect('/me');
    }

    public function leaveParticipant($id)
    {
        $participant = Participant::where('id', $id)->first();

        $participant->users()->detach(Auth::user());
        $participant->delete();

        return redirect('/me');
    }

    //TODO: implement participant delete
}
