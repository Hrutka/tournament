<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TournamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teamSize = 4;
        $descriptionText = 'alma';
        $titleText = 'title';

        for ($i=1; $i < 11; $i++) {
            if ($i % 4 == 0) {
                $teamSize *= 2;
            }

            DB::table('tournaments')->insert([
                'created_at' => Carbon::now(),
                'description' => $descriptionText,
                'owner' => $i,
                'start_date' => Carbon::now(),
                'team_size' => $teamSize,
                'title' => $titleText,
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
