@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row mb-5">
            <div class="col-12">
                @if(count($tournament->participants()->get()) < $tournament->tournament_size)

                <div class="card">
                    <div class="card-header">
                        Jelentkezés
                    </div>

                        <div class="card-body">
                            <form action="{{ url('/') }}/join-tournament/{{ $tournament->id }}" method="post">

                                @csrf

                                <div class="form-group">
                                    <label for="participant">Csapat kiválasztása</label>
                                    <select class="form-control" name="participant" id="participant">
                                        @foreach($userParticipants as $up)
                                            @if(!$tournamentParticipants->where('id', $up->id)->first() and $up->team_size == $tournament->team_size)
                                                <option value="{{ $up->id }}">{{ $up->display_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <button class="btn btn-success" type="submit">Belépés</button>
                            </form>
                        </div>
                </div>
                @endif

            </div>

        </div>
        <div class="row mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Résztvevők
                    </div>
                    <div class="card-content pt-3">
                        <ul>
                            @foreach($tournamentParticipants as $tp)
                                <li>{{ $tp->display_name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                Meccsek
                            </div>
                            @if (count($tournament->matches) == 0 && Auth::user()->id == $tournament->owner)
                                <div class="col text-right">
                                    <a href="{{ route('tournament_generate', ['id' => $tournament->id]) }}" class="btn btn-primary btn-sm">Meccsek generálása</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-content p-3">
                        <div class="row">
                            @foreach($tournament->matches as $match)
                                <form action="{{ url('/') }}/update-match/{{ $match->id }}" method="post" class="col-4 pb-3">

                                    @csrf

                                        <div class="card">
                                            <div class="card-header">
                                                @php $round = $match->round @endphp
                                                [{{ $round }}. kör]
                                                @if (empty($match->homeParticipant))
                                                    @if ($tournament->owner == Auth::user()->id)
                                                        <select class="form-control" name="participant1" id="participant1">
                                                            @foreach($tournament->participants as $up)
                                                                @foreach(\App\Match::where('tournament_id', $tournament->id)->where('round', $round-1)->get() as $matchh)
                                                                    @if($up->id == $matchh->winner)
                                                                        <option value="{{ $up->id }}">{{ $up->display_name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        TBA
                                                    @endif
                                                @else
                                                    {{ $match->homeParticipant->display_name }}
                                                @endif
                                                 vs
                                                @if (empty($match->guestParticipant))
                                                    @if ($tournament->owner == Auth::user()->id)
                                                        <select class="form-control" name="participant2" id="participant2">
                                                            @foreach($tournament->participants as $up)
                                                                @foreach(\App\Match::where('tournament_id', $tournament->id)->where('round', $round-1)->get() as $matchh)
                                                                    @if($up->id == $matchh->winner)
                                                                        <option value="{{ $up->id }}">{{ $up->display_name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        TBA
                                                    @endif
                                                @else
                                                    {{ $match->guestParticipant->display_name }}
                                                @endif
                                            </div>
                                            <div class="card-content p-2">
                                                @if (empty($match->winner))
                                                    <div class="pb-2">
                                                        Ezt a meccset még nem játszották le.
                                                    </div>
                                                    @if ($tournament->owner == Auth::user()->id && !empty($match->homeParticipant) && !empty($match->guestParticipant))
                                                        <label for="winner">Nyertes csapat kiválasztása:</label>
                                                        <select class="form-control mb-3" name="winner" id="winner">
                                                            <option value="{{ $match->homeParticipant->id }}">{{ $match->homeParticipant->display_name }}</option>
                                                            <option value="{{ $match->guestParticipant->id }}">{{ $match->guestParticipant->display_name }}</option>
                                                        </select>
                                                    @endif

                                                    @if ($tournament->owner == Auth::user()->id)
                                                        @if(!empty($match->homeParticipant) && !empty($match->guestParticipant))
                                                            <button class="btn btn-success pt-2 btn-block" type="submit">Meccs lejátszása</button>
                                                        @else
                                                            <button class="btn btn-success pt-2 btn-block" type="submit">Csapatok beállítása</button>

                                                        @endif
                                                    @endif
                                                @else
                                                    Nyertes: {{ \App\Participant::where('id', $match->winner)->first()->display_name }}
                                                @endif
                                            </div>
                                        </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
