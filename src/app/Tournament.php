<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date', 'title', 'description', 'type'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime',
    ];

    /**
     *
     */
    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'participant_tournament');
    }


    public function matches()
    {
        return $this->hasMany(Match::class);
    }
}
