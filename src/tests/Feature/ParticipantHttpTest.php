<?php

namespace Tests\Feature;

use App\Participant;

class ParticipantHttpTest extends BaseAuthenticatedHttpTest
{
    public function testNew()
    {
        $response = $this->get(route('participant_new', ['id' => 0]));
        $response->assertStatus(200);
    }

    public function testSave()
    {
        $participant = factory(Participant::class)->create();
        $participant->id = 1;

        $data = [
            'display_name' => $participant->display_name,
            'team_size' => $participant->team_size,
        ];

        $response = $this->post(route('participant_save', ['id' => 0]), $data);
        $response->assertStatus(302);
    }

    public function testJoin()
    {
        $response = $this->get(route('participant_join', ['id' => 1]));
        $response->assertStatus(302);
    }

    public function testLeave()
    {
        $response = $this->get(route('participant_leave', ['id' => 1]));
        $response->assertStatus(302);
    }

    // TODO: implement participant delete and uncomment this
//    public function testDelete()
//    {
//        $response = $this->get(route('participant_delete', ['id' => 0]));
//        $response->assertStatus(302);
//    }
}
