<?php

namespace App\Http\Controllers;

use App\Participant;
use App\Tournament;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tournaments = Tournament::all();
        $participants = Participant::all();

        return view('home', compact('tournaments', 'participants'));
    }
}
