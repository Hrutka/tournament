<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/me', 'DashboardController@index')->name('dashboard');

Route::get('/new-tournament/{id}', 'TournamentController@addTournament')->name('tournament_new');
Route::post('/save-tournament/{id}', 'TournamentController@saveTournament')->name('tournament_save');
Route::get('/delete-tournament/{id}', 'TournamentController@deleteTournament')->name('tournament_delete');
Route::post('/join-tournament/{id}', 'TournamentController@joinTournament')->name('tournament_join');
Route::get('/tournament/{id}', 'TournamentController@showTournament')->name('tournament_show');
Route::get('/tournament/{id}/generate-matches', 'TournamentController@generateMatches')->name('tournament_generate');

Route::get('/new-participant/{id}', 'ParticipantController@addParticipant')->name('participant_new');
Route::post('/save-participant/{id}', 'ParticipantController@saveParticipant')->name('participant_save');
Route::get('/join-participant/{id}', 'ParticipantController@joinParticipant')->name('participant_join');
Route::get('/leave-participant/{id}', 'ParticipantController@leaveParticipant')->name('participant_leave');
Route::get('/delete-participant/{id}', 'ParticipantController@deleteParticipant')->name('participant_delete');


Route::post('/update-match/{id}', 'MatchController@update')->name('match_update');

