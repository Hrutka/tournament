<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->foreignId('participant1_id')
                ->nullable();
            $table->foreign('participant1_id')
                ->references('id')
                ->on('participants');
            $table->foreignId('participant2_id')
                ->nullable();
            $table->foreign('participant2_id')
                ->references('id')
                ->on('participants');
            $table->foreignId('tournament_id');
            $table->foreign('tournament_id')
                ->references('id')
                ->on('tournaments');
            $table->integer('round');
            $table->smallInteger('winner')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
