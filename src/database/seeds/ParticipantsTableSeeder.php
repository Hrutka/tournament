<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParticipantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teamSize = 5;

        for ($i = 1; $i < 11; $i++) {
            if ($i % 4 == 0) {
                $teamSize--;
            }

            DB::table('participants')->insert([
                'created_at' => Carbon::now(),
                'display_name' => 'Participant '.$i,
                'team_size' => $teamSize,
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
