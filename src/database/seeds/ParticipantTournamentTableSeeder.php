<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParticipantTournamentTableSeeder extends Seeder
{
    private static $tournament = [5, 5, 5, 5, 5, 5, 5, 5];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 8; $i++) {
            DB::table('participant_tournament')->insert([
                'participant_id' => $i+1,
                'tournament_id' => self::$tournament[$i],
                'order' => $i,
            ]);
        }
    }
}
