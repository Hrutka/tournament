<?php


namespace App\Http\Controllers;


use App\Match;
use App\Participant;
use App\Tournament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class TournamentController extends Controller
{

    public function addTournament($id)
    {
        if ($id == 0) {
            return view('add_tournament');

        } else {
            $tournament = Tournament::where('id', $id)->first();

            return view('add_tournament', compact('id', 'tournament'));
        }

    }

    public function generateMatches(Request $request, $id)
    {
        $tournament = Tournament::where('id', $id)->firstOrFail();

        if ($tournament->owner !== Auth::user()->id) {
            throw new AccessDeniedException("Nem megfelelő jogosultság.");
        }

        if (count($tournament->matches) !== 0) {
            return redirect()->back()->with('status', 'Már vannak meccsek generálva.');
        }

        if (count($tournament->participants) !== $tournament->tournament_size) {
            return redirect()->back()->with('status', sprintf('Még %s csapat jelentkezésére várunk.', $tournament->tournament_size - count($tournament->participants)));
        }

        $counter = $tournament->tournament_size / 2;
        $level = 1;
        $participantArray = $tournament->participants()->select('id')->get()->toArray();

//        dd($participantArray);

        while($counter >= 1) {
            $matches = [];

            for($i = 0; $i < $counter; $i++) {
                if ($level == 1) {
                    $matches[] = new Match(['round' => $level, 'participant1_id' => $participantArray[$i]['id'], 'participant2_id' => $participantArray[$i+$counter]['id']]);
                } else {
                    $matches[] = new Match(['round' => $level]);

                }
            }

            $tournament->matches()->saveMany($matches);

            $level++;
            $counter /= 2;
        }

        return redirect()->back();
    }

    public function saveTournament(Request $request, $id = 0)
    {

        if ($id == 0) {
            $tournament = new Tournament();
        } else {
            $tournament = Tournament::where('id', $id)->first();
        }

        $tournament->title = $request->title;
        $tournament->description = $request->description;
        $tournament->team_size = $request->team_size;
        $tournament->start_date = $request->start_date;
        $tournament->tournament_size = $request->tournament_size;
        $tournament->owner = Auth::user()->id;

        $tournament->save();

        return redirect('/');

    }

    public function deleteTournament($id)
    {
        $tournament = Tournament::where('id', $id)->first();

        if(Auth::id() != $tournament->owner)
            return back();

        $tournament->owner = 0;

        $tournament->save();

        return redirect('/');
    }

    public function joinTournament(Request $request, $id)
    {
        $participant = Participant::where('id', $request->participant)->first();
        $tournament = Tournament::where('id', $id)->first();

        $tournament->participants()->save($participant);

        return back();
    }

    public function showTournament($id)
    {
        $tournament = Tournament::where('id', $id)->first();
        $userParticipants = Auth::user()->participants()->get();
        $tournamentParticipants = $tournament->participants()->get();

        return view('tournament', compact('tournament', 'userParticipants', 'tournamentParticipants'));

    }
}
