<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateDatabase();
        $this->call(UsersTableSeeder::class);
        $this->call(TournamentsTableSeeder::class);
        $this->call(ParticipantsTableSeeder::class);
        //$this->call(MatchesTableSeeder::class);
        //$this->call(UserParticipantTableSeeder::class);
        //$this->call(ParticipantTournamentTableSeeder::class);


       // DB::table('users')->insert([
       //     'created_at' => \Carbon\Carbon::now(),
       //     'email' => Str::random(10).'@gmail.com',
       //     'email_verified_at' => NULL,
       //     'name' => Str::random(10),
       //     'password' => Hash::make('password'),
       //     'remember_token' => Str::random(10),
       //     'updated_at' => \Carbon\Carbon::now(),
       // ]);
        //factory(App\User::class, 10)->create();
    }

    private function truncateDatabase(): void
    {
        Schema::disableForeignKeyConstraints();

        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tableNames as $name) {
            if ($name == 'migrations') {
                continue;
            }
            DB::table($name)->truncate();
        }

        Schema::enableForeignKeyConstraints();

    }
}
