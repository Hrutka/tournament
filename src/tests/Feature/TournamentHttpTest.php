<?php

namespace Tests\Feature;

use App\Tournament;
use Illuminate\Support\Facades\Auth;

class TournamentHttpTest extends BaseAuthenticatedHttpTest
{
    public function testNew()
    {
        $response = $this->get(route('tournament_new', ['id' => 0]));
        $response->assertStatus(200);
    }

    public function testSave()
    {
        $tournament = factory(Tournament::class)->create();
        $tournament->owner = Auth::user();

        $data = [
            'title' => $tournament->title,
            'description' => $tournament->description,
            'start_date' => $tournament->start_date,
            'team_size' => $tournament->team_size,
            'id' => $tournament->id,
            'owner' => $tournament->owner,
        ];

        $response = $this->post(route('tournament_save', ['id' => $tournament->id]), $data);
        $response->assertStatus(302);
    }

    public function testDelete()
    {
        $tournament = factory(Tournament::class)->create();
        $tournament->owner = Auth::user();

        $response = $this->get(route('tournament_delete', ['id' => $tournament->id]));
        $response->assertStatus(302);
    }
}
