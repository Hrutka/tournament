@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Csapat hozzáadása</div>

                    <div class="card-body">
                        <form action="/save-participant/{{ $id ?? '0' }}" method="post">

                            @csrf

                            <div class="form-group">
                                <label for="display-name">Csapat név</label>
                                <input type="text" class="form-control" id="display_name"
                                       name="display_name" aria-describedby="display-name"
                                       required
                                       value="{{ $participant->display_name ?? '' }}">
                            </div>

                            <div class="form-group">
                                <label for="team-size">Csapat mérete</label>
                                <select class="form-control" id="team-size" name="team_size" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Létrehozás</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
