@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                Bajnokságok
                            </div>
                            <div class="col text-right">
                                <a href="/new-tournament/0" class="btn btn-primary btn-sm">Új bajnokság</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (count($tournaments) === 0)
                            <p>Jelenleg nincs egy aktív tournament sem.</p>
                        @else
                            @foreach($tournaments as $tournament)
                                @if($tournament->owner != 0)
                                <div class="row">
                                    <div class="col-8">
                                        <div>
                                            <a href="{{ url('/') }}/tournament/{{ $tournament->id }}">
                                                <strong>{{ $tournament->title }}</strong> @ {{ $tournament->start_date }}

                                            </a>
                                        </div>
                                        <div>(résztvevők: {{ $tournament->participants()->count() }})</div>
                                    </div>
                                    <div class="col-4 mb-2">

                                            @if(\Illuminate\Support\Facades\Auth::id() == $tournament->owner)
                                                <a href="/new-tournament/{{ $tournament->id }}" class="btn btn-secondary btn-sm btn-block">Bajnokság szerkesztése</a>
                                                <a href="/delete-tournament/{{ $tournament->id }}" class="btn btn-danger btn-sm btn-block">Bajnokság törlése</a>
                                            @endif

                                    </div>
                                </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                Csapatok
                            </div>
                            <div class="col text-right">
{{--                                <a href="/new-tournament/0" class="btn btn-primary btn-sm">Új bajnokság</a>--}}
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (count($participants) === 0)
                            <p>Jelenleg nincs egy aktív tournament sem.</p>
                        @else
                            @foreach($participants as $participant)
                                <div class="row">
                                    <div class="col-8">
                                        <div>
                                            <strong>{{ $participant->display_name }}</strong>
                                        </div>
                                        <div>(résztvevők: {{ $participant->users()->count() }})</div>
                                    </div>

                                    @if(count($participant->users()->find(Auth::user())) == 0)
                                        @if(count($participant->users()->get()) < $participant->team_size )
                                            <div class="col-4 mb-2">
                                                <a href="/join-participant/{{ $participant->id }}" class="btn btn-secondary btn-sm btn-block">Csapatba jelentkezés</a>
                                            </div>
                                        @endif
                                    @endif

                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
