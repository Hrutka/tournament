<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateSafely extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:safely';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations after setup only in fat and prod environment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (env('MIGRATE_ON_SETUP', false)) {
            $this->callSilent('migrate');
            $this->callSilent('db:seed');
        }
    }
}
