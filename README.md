# Tournament


## Alkalmazás bemutatója
A _Tournament app_ segítségével lebonyolíthatod a saját, több csapatos bajnokságodat.
Az alkalmazás segít a jelentkezők adminisztrálásában, a meccsek generálásában és az eredmények feljegyzésében. Mindehhez egy egyszerűen használható felületet biztosít.

Minden regisztrált felhasználó indíthat bajnokságot, létrehozhat csapatot valamit beléphet csapatokba és kiléphet belőlük.
Az elindított bajnokságot a tulajdonos módosíthatja és törölheti is.

Bajnokságot a főoldalról indíthatunk, az __Új bajnokság__ gomb segítségével. 
A bajnokság elindításakor meg kell adnunk a bajnokság nevét, leírását, a résztvevő csapatok méretét (1-5), a bajnokság méretét, és az indulás időpontját.
Ezek az értékek később módosíthatóak.
![Alt text](screenshots/bajnoksagletrehozas.jpg?raw=true title="bajnoksag letrehozas")

A bejelentkezett felhasználó a jobb felső sarokban lévő legördülő menü segítségével eljuthat a __Dashboad__-jára, ahol megtekintheti a szamélyes adatait, csapatait és mérkőzéseit.
![Alt text](screenshots/dashboard01.jpg?raw=true title="dashboard")

Itt nyílik lehetőség új csapat létrehozására az __új csapat__ gomb lenyomásával és kilépni az aktuális csapatainkból a __Kilépés__ gombbal. 
A csapat létrehozásához csak a csapat nevét és a méretét kell megadni. 
![Alt text](screenshots/createteam01.jpg?raw=true title="uj csapat)

A főoldalon láthatjuk az aktuális bajnokságok és csapatok listáját. Minden olyan csapatba jelentkezhetünk innen amelyiknek még nem vagyunk a tagjai és nem telt még meg.
A bajnokságok listájában a saját bajnokságaink mellett megjelennek a __Bajnoság szerkesztése__ és a __Bajnokság törlése__ gombok.
![Alt text](screenshots/homepage01.jpg?raw=true title="homepage")

A bajnokságok nevére rákattintva láthatjuk a bajnokság részleteit. Megjelennek a résztvevők nevei, a meccsek és a jelentkezés kártya.
Jelentkezés előtt ki kell választanunk, hogy melyik csapatunkkal szeretnénk jelentkezni a bajnokságba, majd a __Belépés__ gombra kattintani.
Ezen az oldalon láthatjuk a véget ért meccsek eredményeit és azt, hogy az adott mérkőzés a bajnokság melyik körében zajlott le.
![Alt text](screenshots/tournament01.jpg?raw=true title="mas felhasznalo bajnmoksaga")

Amennyiben a saját bajnokságunk nevére kattintunk a meccsek kártya fejlécében megjelenik egy __Meccsek generálása__ gomb.
Meccseket csak akkor tudunk generálni, ha a jelentkezők létszáma elérte az induláshoz szükséges minimumot. Különben egy figyelmeztető üzenetet kapunk.
![Alt text](screenshots/figyelmeztetes.jpg?raw=true title="figyemlemztetés")

Amint a jelentkezők száma megfelelő, legenerálhatjuk az 1.kör meccseit, amelyek megjelennek a meccsek kártyán.
![Alt text](screenshots/generaltmeccsek01.jpg?raw=true title="generalt meccsek 1.kor")

Amint a felek lejátszották a meccseiket a nyertes csapat kiválasztásával és a __Meccs lejátszása__ gomb segítségével adminisztrálhatjuk az eredményt.
Ez után összeállítjuk a következő kör meccseit és a __Csapatok beállítása__ gombbal létrehozzuk a kövekező kört.
![Alt text](screenshots/generaltmeccsek02.jpg?raw=true title="generalt meccsek 2.kor")

Ezt folytathatjuk a bajnokság végéig, amikor már nem marad más dolgunk mint kihirdetni a nyertest.

Kijelentkezni a jobb felő sarokban lévő legördülő menüben lévő __Logout__ gomb segítségével tudunk.



## Követelmények
- [PHP](https://www.php.net/manual/en/install.php)
- [Composer](https://getcomposer.org/download/)
- [Node.js](https://nodejs.org/en/download/), [npm](https://www.npmjs.com/get-npm)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Használat
### Telepítés
```
$ ./build.sh
```

### Docker konténerek elindítása
```
$ ./run.sh
```
Első indítás után a `$ ./db.sh` parancsot is le kell futtatni az adatbázis feltöltéséhez.

Ha minden oké, akkor böngészőből elérhető a `http://localhost` címen.

### Test futtatás
```
$ ./test.sh
```

## Deploy

### FAT

Develop branchen lévő kód: http://tournament-bead-fat.herokuapp.com/

### PROD

Master branchen lévő kód: http://tournament-bead-prod.herokuapp.com/
