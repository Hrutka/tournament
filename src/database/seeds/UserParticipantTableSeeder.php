<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserParticipantTableSeeder extends Seeder
{
    private static $user = [3, 5, 2, 8, 6, 1, 7, 10, 3, 2];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('user_participant')->insert([
                'participant_id' => $i+1,
                'user_id' => self::$user[$i],
            ]);
        }
    }
}
