<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use mysql_xdevapi\Exception;
use Tests\DuskTestCase;

class TournamentTest extends DuskTestCase
{
    private $name = 'testtournament01';
    private $date = '2020-01-01 01:01:01';
    private $description = 'best tournamnet for test';
    private $size = 4;

    private $name1 = 'oldtournament';
    private $date1 = '1997-01-01 01:01:01';
    private $description1 = 'an old tournament';
    private $size1 = 2;


    /**
     *
     * @test
     * @return void
     * @throws \Throwable
     */
    public function createTournamentTest()
    {

        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            try {
                $browser->LoginAs($user->email)
                    ->visit('/')
                    ->assertSee('Bajnokságok')
                    ->assertSee('Csapatok')
                    ->clickLink('Új bajnokság')
                    ->assertPathIs('/new-tournament/0')
                    ->assertSee('Bajnokság hozzáadása')
                    ->type('title', $this->name)
                    ->type('description', $this->description)
                    ->select('team_size', $this->size)
                    ->type('start_date', $this->date)
                    ->press('Létrehozás')
                    ->assertPathIs('/')
                    ->assertSee($this->name . ' @ ' . $this->date)
                    ->assertSee('(résztvevők: 0)')
                    ->assertSeeLink('Bajnokság szerkesztése')
                    ->assertSeeLink('Bajnokság törlése');
            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $user->delete();
            }
        });
    }

    /**
     *
     * @test
     * @return void
     * @throws \Throwable
     */
    public function modifyDeleteTournamentTest()
    {

        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            try {
                $browser->LoginAs($user->email)
                    ->visit('/')
                    ->clickLink('Új bajnokság')
                    ->type('title', $this->name)
                    ->type('description', $this->description)
                    ->select('team_size', $this->size)
                    ->type('start_date', $this->date)
                    ->press('Létrehozás')
                    ->clickLink('Bajnokság szerkesztése')
                    ->type('title', $this->name1)
                    ->type('description', $this->description1)
                    ->select('team_size', $this->size1)
                    ->type('start_date', $this->date1)
                    ->press('Létrehozás')
                    ->assertPathIs('/')
                    ->assertSee($this->name1 . ' @ ' . $this->date1)
                    ->assertSee('(résztvevők: 0)')
                    ->assertSeeLink('Bajnokság szerkesztése')
                    ->assertSeeLink('Bajnokság törlése')
                    ->clickLink('Bajnokság törlése')
                    ->assertPathIs('/')
                    ->assertDontSee($this->name1 . ' @ ' . $this->date1);

            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $user->delete();
            }
        });
    }
}
