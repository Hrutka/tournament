<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantTournamentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_tournament', function (Blueprint $table) {
            $table->foreignId('participant_id');
            $table->foreign('participant_id')
                ->references('id')
                ->on('participants');
            $table->foreignId('tournament_id');
            $table->foreign('tournament_id')
                ->references('id')
                ->on('tournaments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_tournament');
    }
}
