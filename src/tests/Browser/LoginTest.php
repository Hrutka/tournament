<?php

namespace Tests\Browser;


use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Laravel\Dusk\Browser;
use mysql_xdevapi\Exception;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{

    /**
     * A Dusk test example.
     *
     * @test
     * @return void
     * @throws \Throwable
     */
    public function loginLogoutWithRegisteredUser()
    {
        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            try {
                $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'password')
                    ->press('Login')
                    ->assertPathIs('/')
                    ->clickLink($user->name)
                    ->clickLink('Logout')
                    ->assertPathIs('/login');
            } catch (Exception $e) {
                echo $e->getMessage();
            } finally {
                $user->delete();
            }
        });
    }

    /**
     *
     *
     *
     * @return void
     * @throws \Throwable
     */
    public function loginWithNotRegisteredUser()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('email', 'notregistereduser@test.test')
                ->type('password', 'password')
                ->press('Login')
                ->assertSee('These credentials do not match our records.')
                ->assertPathIs('/login');
        });
    }
}
