# Tournament
## Testing

### Browser testing
Setup:
- Add chrome to docker-compose.yml:


    chrome:
         image: robcherry/docker-chromedriver
         networks:
            - backend
            - frontend
         environment:
            CHROMEDRIVER_WHITELISTED_IPS: ""
            CHROMEDRIVER_PORT: "9515"
         ports:
            - 9515:9515
         cap_add:
            - "SYS_ADMIN"
- `composer require --dev laravel/dusk`
- Register the DuskProvider inside our AppServiceProvider


    public function register()
     {
         if ($this->app->environment('local','testing environment name'){
           $this->app->register(\Laravel\Dusk\DuskServiceProvider);
         }
     }
-  `php artisan dusk:install`
- create .env.dusk file. Set
    `APP_URL=http://tournament_nginx_1` (container name)
- Inside the tests directory we will find the **DuskTestCase** class,
    now we need to change the path of the **RemoteWebDriver**
    in `http://{{docker-services-name}}:{{services-port}}`, in our case `http://chrome:9515`
- Make test files: `docker-compose run php-fpm php artisan dusk:make TestName`
- Run tests: `docker-compose run php-fpm php artisan dusk` or  `docker-compose run php-fpm php artisan test`

Tests:
- Login/logout tests
- Create/modify/delete tournament
- Join/leave team
- Basic text checks
- ULR checks

### Http testing

Run http tests with
`docker-compose -f docker/docker-compose.yml --project-directory=docker run php-fpm php artisan test`

Tests:
- Tournament new endpoint
- Tournament save endpoint
- Tournament delete endpoint
- Participant new endpoint
- Participant save endpoint
- Join to participant endpoint
- Leave from participant endpoint
- Participant delete endpoint
