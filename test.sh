echo "Running http tests:"
docker-compose -f docker/docker-compose.yml --project-directory=docker run php-fpm php artisan test
echo "Running browser tests:"
docker-compose -f docker/docker-compose.yml --project-directory=docker run php-fpm php artisan dusk
