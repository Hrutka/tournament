<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Participant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name'
    ];

    /**
     *
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_participant');
    }

    /**
     *
     */
    public function tournaments()
    {
        return $this->belongsToMany(Tournament::class, 'participant_tournament')->withPivot('order');
    }

    /**
     *
     */
    public function matches()
    {
        return $this->homeMatches()->get()->merge(
          $this->guestMatches()->get()
        );
    }

    /**
     *
     */
    public function homeMatches()
    {
        return $this->hasMany(Match::class, 'participant1_id');
    }

    /**
     *
     */
    public function guestMatches()
    {
        return $this->hasMany(Match::class, 'participant2_id');
    }

}
