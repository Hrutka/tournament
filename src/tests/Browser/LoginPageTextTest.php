<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\User;

class LoginPageTextTest extends DuskTestCase
{
    /**
     * Login page text test
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Tournament')
                    ->assertSee('Login')
                    ->assertSee('Register')
                    ->assertSee('Remember Me')
                    ->assertSee('Forgot Your Password?')
                    ->assertSee('E-Mail Address');
        });
    }
}
