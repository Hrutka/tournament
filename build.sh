echo "Building project.."
cd src
cp .env.example .env
composer install --ignore-platform-reqs
npm install
npm run prod
php artisan key:generate
cd ..
docker-compose -f docker/docker-compose.yml --project-directory=docker build
echo "Build done."
