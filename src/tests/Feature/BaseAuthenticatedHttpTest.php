<?php


namespace Tests\Feature;


use App\User;

abstract class BaseAuthenticatedHttpTest extends BaseHttpTest
{
    protected function setUser(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUser();
    }
}
